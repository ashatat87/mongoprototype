const express = require('express');
const advancedResults = require('../middleware/advancedResults');
const { Timebarsmetadata } = require('../models');
const {
	createTbMetadata,
	getTbMetadatas,
	deleteTbMetadatas,
	getTbMetadata,
	updateTbMetadata,
	deleteTbMetadata,
  getTbMetadataByTbMdid,
  deleteTbMetadataBytbMdid
} = require('../controllers/timebarsmetadata');
const { protect } = require('../middleware/auth');

const router = express.Router();


router
	.route('/')
	.post(protect, createTbMetadata)
	.delete(protect, deleteTbMetadatas)
	.get(advancedResults(Timebarsmetadata), getTbMetadatas);

router
	.route('/:id')
	.get(getTbMetadata)
	.put(updateTbMetadata)
	.delete(protect, deleteTbMetadata);

router
	.route('/tbmdid/:tbmdid')
	.get(getTbMetadataByTbMdid)
	.delete(protect, deleteTbMetadataBytbMdid);

module.exports = router;
