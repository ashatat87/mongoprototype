const express = require('express');
const advancedResults = require('../middleware/advancedResults');
const { Timebarsbaseline } = require('../models');
const {
	createTbBaseline,
	getTbBaselines,
	deleteTbBaselines,
	getTbBaseline,
	updateTbBaseline,
	deleteTbBaseline,
  getTbBaselineByTbid,
  deleteTbBaselineBytbid
} = require('../controllers/timebarsbaseline');
const { protect } = require('../middleware/auth');

const router = express.Router();


router
	.route('/')
	.post(protect, createTbBaseline)
	.delete(protect, deleteTbBaselines)
	.get(advancedResults(Timebarsbaseline), getTbBaselines);

router
	.route('/:id')
	.get(getTbBaseline)
	.put(updateTbBaseline)
	.delete(protect, deleteTbBaseline);

router
	.route('/tbid/:tbid')
	.get(getTbBaselineByTbid)
	.delete(protect, deleteTbBaselineBytbid);

module.exports = router;
