const express = require('express');
const advancedResults = require('../middleware/advancedResults');
const { Timebarsrescalcs } = require('../models');
const {
	createTbRescalc,
	getTbRescalcs,
	deleteTbRescalcs,
	getTbRescalc,
	updateTbRescalc,
	deleteTbRescalc,
  getTbRescalcByTbrescalcid,
  deleteTbRescalcByTbresclacid
} = require('../controllers/timebarsrescalcs');
const { protect } = require('../middleware/auth');

const router = express.Router();


router
	.route('/')
	.post(protect, createTbRescalc)
	.delete(protect, deleteTbRescalcs)
	.get(advancedResults(Timebarsrescalcs), getTbRescalcs);

router
	.route('/:id')
	.get(getTbRescalc)
	.put(updateTbRescalc)
	.delete(protect, deleteTbRescalc);

router
	.route('/tbrescalcid/:tbrescalcid')
	.get(getTbRescalcByTbrescalcid)
	.delete(protect, deleteTbRescalcByTbresclacid);

module.exports = router;
