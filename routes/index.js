const router = require('express').Router();

const auth = require('./auth');
const tbadminpanel = require('./timebarsadminpanel');
const tbbaseline = require('./timebarsbaseline');
const tbdocuments = require('./timebarsdocs');
const tbcore = require('./timebarscore');
const tbmetadata = require('./timebarsmetadata');
const tbrescalcs = require('./timebarsrescalcs');
const tbresources = require('./timebarsresources');
const tbtags = require('./timebarstags');
const users = require('./users');

router.use('/auth', auth);
router.use('/tbadminpanel', tbadminpanel);
router.use('/tbbaseline', tbbaseline);
router.use('/tbcore', tbcore);
router.use('/tbdocuments', tbdocuments);
router.use('/tbmetadata', tbmetadata);
router.use('/tbrescalcs', tbrescalcs);
router.use('/tbresources', tbresources);
router.use('/tbtags', tbtags);
router.use('/users', users);

module.exports = router;
