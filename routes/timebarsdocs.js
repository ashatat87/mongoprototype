const express = require('express');
const advancedResults = require('../middleware/advancedResults');
const { Timebarsdocs } = require('../models');
const {
	createTbDocs,
	getTbDocs,
	deleteTbDocs,
	getTbDoc,
	updateTbDoc,
	deleteTbDoc,
	getTbDocByTbdocid,
	deleteTbDocByTbdocid
} = require('../controllers/timebarsdocs');
const { protect } = require('../middleware/auth');

const router = express.Router();

// @route   GET /tbdocuments/test
// @desc    Tests Timebardocuments route
// @access  Public
router.get('/test', (req, res) => res.json({ msg: 'Timebardocuments route Works' }));

router
	.route('/')
	.post(protect, createTbDocs)
	.delete(protect, deleteTbDocs)
	.get(advancedResults(Timebarsdocs), getTbDocs);

router
	.route('/:id')
	.get(getTbDoc)
	.put(updateTbDoc)
	.delete(protect, deleteTbDoc);

router
	.route('/tbdocid/:tbdocid')
	.get(getTbDocByTbdocid)
	.delete(protect, deleteTbDocByTbdocid);

module.exports = router;
