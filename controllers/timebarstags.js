const asyncHandler = require('../middleware/async');
const { Timebarstags } = require('../models');
const ErrorResponse = require('../utils/errorResponse');

// @route   POST /api/v1/tbtags/   
// @desc    Create tbtags
// @access  Private
exports.createTbTags = asyncHandler( async (req, res) => {
  const {
    tbTagID,
    tbTagCustomerID,
    tbTagAzureID,
    tbTagGroup,
    tbTagName,
    tbTagNameShort,
    tbTagPurpose,
    tbTagDescription,
    tbTagOwner,
    tbTagPopular,
    tbTagSortOrder,
    tbTagLastModified,
    tbTagEntity
  } = req.body;

  const newTag = await Timebarstags.create({
    tbTagID,
    tbTagCustomerID,
    tbTagAzureID,
    tbTagGroup,
    tbTagName,
    tbTagNameShort,
    tbTagPurpose,
    tbTagDescription,
    tbTagOwner,
    tbTagPopular,
    tbTagSortOrder,
    tbTagLastModified,
    tbTagEntity
  });

  res.status(201).json({
    success: true,
    data: newTag
  });
});

// @route   GET /api/v1/tbtags/
// @desc    Get all tbtags 
// @access  Public
exports.getTbTags = asyncHandler( async (req, res) => {
  res.json(res.advancedResults);
});

// @route DELETE /api/v1/tbtags/
// @desc DELETE all tbtags
// @access Private
exports.deleteTbTags = asyncHandler( async (req, res, next) => {
  const deletedTbTags = await Timebarstags.deleteMany();

  if (!deletedTbTags.deletedCount) return next(new ErrorResponse('No tbtags found', 404));

  res.json({
    success: true,
    data: {}
  })
});

// @route   GET /api/v1/tbtags/:id
// @desc    Get one tbtags by id
// @access  Public
exports.getTbTag = asyncHandler( async (req, res, next) => {
  const tbTag = await Timebarstags.findById(req.params.id);

  if (!tbTag) return next(new ErrorResponse('Resource not found', 404));

  res.json({
    success: true,
    data: tbTag
  });
});

// @route   PUT /api/v1/tbtags/:id
// @desc    Update on one tbtags by id
// @access  Public
exports.updateTbTag = asyncHandler( async (req, res, next) => {
  const updatedTbTag = await Timebarstags.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true
  });

  if (!updatedTbTag) return next(new ErrorResponse('Resource not found', 404));

  res.json({
    success: true,
    data: updatedTbTag
  });
});

// @route   DELETE /tbtags/:id
// @desc    Delete tbtags by id
// @access  Private
exports.deleteTbTag = asyncHandler( async (req, res, next) => {
  const deletedTbTag = await Timebarstags.findByIdAndDelete(req.params.id);

  if (!deletedTbTag) return next(new ErrorResponse('Resource not found', 404));

  res.json({
    success: true,
    data: {}
  });
});

// @route GET /api/v1/tbtags/tbtagid/:tbtagid
// @desc Get tbtags by tbTagID
// @access  Public
exports.getTbTagByTbtagid = asyncHandler( async (req, res, next) => {
  const tbTag = await Timebarstags.findOne({ tbTagID: req.params.tbtagid });

  if (!tbTag) return next(new ErrorResponse('Resource not found', 404));

  res.json({
    success: true,
    data: tbTag
  });
});

// @route DELETE /tbtags/tbtagid/:tbtagid
// @desc DELETE tbtags by tbTagID
// @access  Private
exports.deleteTbTagByTbtagid = asyncHandler( async (req, res, next) => {
  const deletedTbTag = await Timebarstags.findOneAndDelete({ tbTagID: req.params.tbtagid });

  if (!deletedTbTag) return next(new ErrorResponse('Resource not found', 404));

  res.json({
    success: true,
    data: {}
  });
});
