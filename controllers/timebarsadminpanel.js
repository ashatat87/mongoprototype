const asyncHandler = require('../middleware/async');
const { Timebarsadminpanel } = require('../models');
const ErrorResponse = require('../utils/errorResponse');

// @route   POST /api/v1/tbadminpanel/   
// @desc    Create tbadminpanel
// @access  Private
exports.createTbAdminpanel = asyncHandler( async (req, res) => {
  const {
    apID,
    apCustomerID,
    apAzureID,
    ap_name,
    apTS_WeeklyOrMonthly,
    apTS_Start,
    apTS_Finish,
    apTS_MonthlyPxFactor,
    apTS_WeeklyPxFactor,
    apHelpCoordTop,
    apHelpCoordLeft,
    apStatusDate,
    ap_HolidayDate,
    ap_HolidayName,
    totalNumberOfPeople,
    numberOfPeoplePerLine,
    pxPerPerson,
    apFilteredFrom,
    apFilteredFromTbID,
    apSchMethod,
    apTbVisibleLevel,
    hideCompletedBarsYN
  } = req.body;

  const newTbAdminpanel = await Timebarsadminpanel.create({
    apID,
    apCustomerID,
    apAzureID,
    ap_name,
    apTS_WeeklyOrMonthly,
    apTS_Start,
    apTS_Finish,
    apTS_MonthlyPxFactor,
    apTS_WeeklyPxFactor,
    apHelpCoordTop,
    apHelpCoordLeft,
    apStatusDate,
    ap_HolidayDate,
    ap_HolidayName,
    totalNumberOfPeople,
    numberOfPeoplePerLine,
    pxPerPerson,
    apFilteredFrom,
    apFilteredFromTbID,
    apSchMethod,
    apTbVisibleLevel,
    hideCompletedBarsYN
  });

  res.status(201).json({
    success: true,
    data: newTbAdminpanel
  });
});

// @route   GET /api/v1/tbadminpanel/
// @desc    Get all tbadminpanel 
// @access  Public
exports.getTbAdminpanels = asyncHandler( async (req, res) => {
  res.json(res.advancedResults);
});

// @route DELETE /api/v1/tbadminpanel/
// @desc DELETE all tbadminpanel
// @access Private
exports.deleteTbAdminpanels = asyncHandler( async (req, res, next) => {
  const deletedTbAdminpanels = await Timebarsadminpanel.deleteMany();

  if (!deletedTbAdminpanels.deletedCount) return next(new ErrorResponse('No tbadminpanel found', 404));

  res.json({
    success: true,
    data: {}
  })
});

// @route   GET /api/v1/tbadminpanel/:id
// @desc    Get one tbadminpanel by id
// @access  Public
exports.getTbAdminpanel = asyncHandler( async (req, res, next) => {
  const tbAdminpanel = await Timebarsadminpanel.findById(req.params.id);

  if (!tbAdminpanel) return next(new ErrorResponse('Resource not found', 404));

  res.json({
    success: true,
    data: tbAdminpanel
  });
});

// @route   PUT /api/v1/tbadminpanel/:id
// @desc    Update on one tbadminpanel by id
// @access  Public
exports.updateTbAdminpanel = asyncHandler( async (req, res, next) => {
  const updatedTbAdminpanel = await Timebarsadminpanel.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true
  });

  if (!updatedTbAdminpanel) return next(new ErrorResponse('Resource not found', 404));

  res.json({
    success: true,
    data: updatedTbAdminpanel
  });
});

// @route   DELETE /api/v1/tbadminpanel/:id
// @desc    Delete tbadminpanel by id
// @access  Private
exports.deleteTbAdminpanel = asyncHandler( async (req, res, next) => {
  const deletedTbAdminpanel = await Timebarsadminpanel.findByIdAndDelete(req.params.id);

  if (!deletedTbAdminpanel) return next(new ErrorResponse('Resource not found', 404));

  res.json({
    success: true,
    data: {}
  });
});

// @route GET /api/v1/tbadminpanel/apid/:apid
// @desc Get tbadminpanel by apID
// @access  Public
exports.getTbAdminpanelByApid = asyncHandler( async (req, res, next) => {
  const tbAdminpanel = await Timebarsadminpanel.findOne({ apID: req.params.apid });

  if (!tbAdminpanel) return next(new ErrorResponse('Resource not found', 404));

  res.json({
    success: true,
    data: tbAdminpanel
  });
});

// @route DELETE /api/v1/tbadminpanel/apid/:apid
// @desc DELETE tbadminpanel by apID
// @access  Private
exports.deleteTbAdminpanelByApid = asyncHandler( async (req, res, next) => {
  const deletedTbAdminpanel = await Timebarsadminpanel.findOneAndDelete({ apID: req.params.apid });

  if (!deletedTbAdminpanel) return next(new ErrorResponse('Resource not found', 404));

  res.json({
    success: true,
    data: {}
  });
});
