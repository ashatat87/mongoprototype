const asyncHandler = require('../middleware/async');
const { Timebarsdocs } = require('../models');
const ErrorResponse = require('../utils/errorResponse');

// @route   POST /api/v1/tbdocuments/   
// @desc    Create tbdocuments
// @access  Private
exports.createTbDocs = asyncHandler(async (req, res) => {
  const {
    tbDocID,
    tbDocName,
    tbDocCustomerID,
    tbDocBriefDescription,
    tbDocType,
    tbDocNameShort,
    tbDocHTMLContent,
    tbDocGroup,
    tbDocPurpose,
    tbDocOwner,
    tbDocPopular,
    tbDocSortOrder,
    tbDocLastModified,
    tbDocApprovedBy,
    tbDocStatus,
    tbDocWrittenBy,
  } = req.body;

  const newDoc = await Timebarsdocs.create({
    tbDocID,
    tbDocName,
    tbDocCustomerID,
    tbDocBriefDescription,
    tbDocType,
    tbDocNameShort,
    tbDocHTMLContent,
    tbDocGroup,
    tbDocPurpose,
    tbDocOwner,
    tbDocPopular,
    tbDocSortOrder,
    tbDocLastModified,
    tbDocApprovedBy,
    tbDocStatus,
    tbDocWrittenBy,
  });
  res.status(201).json({
    success: true,
    data: newDoc
  });
});

// @route   GET /api/v1/tbdocuments/
// @desc    Get all tbdocuments 
// @access  Public
exports.getTbDocs = asyncHandler(async (req, res) => {
  res.json(res.advancedResults);
});

// @route DELETE /api/v1/tbdocuments/
// @desc DELETE all tbdocuments
// @access Private
exports.deleteTbDocs = asyncHandler(async (req, res, next) => {
  const deletedTbDocs = await Timebarsdocs.deleteMany();

  if (!deletedTbDocs.deletedCount) return next(new ErrorResponse('No tbdocuments found', 404));

  res.json({
    success: true,
    data: {}
  });
});

// @route   GET /api/v1/tbdocuments/:id
// @desc    Get one tbdocuments by id
// @access  Public
exports.getTbDoc = asyncHandler(async (req, res, next) => {
  const tbDoc = await Timebarsdocs.findById(req.params.id);

  if (!tbDoc) return next(new ErrorResponse('Resource not found', 404));

  res.json({
    success: true,
    data: tbDoc
  });
});

// @route   PUT /api/v1/tbdocuments/:id
// @desc    Update on one tbdocuments by id
// @access  Public
exports.updateTbDoc = asyncHandler(async (req, res, next) => {
  const updatedTbDoc = await Timebarsdocs.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true
  });

  if (!updatedTbDoc) return next(new ErrorResponse('Resource not found', 404));

  res.json({
    success: true,
    data: updatedTbDoc
  });
});

// @route   DELETE /tbdocuments/:id
// @desc    Delete tbdocuments by id
// @access  Private
exports.deleteTbDoc = asyncHandler(async (req, res, next) => {
  const deletedTbDoc = await Timebarsdocs.findByIdAndDelete(req.params.id);

  if (!deletedTbDoc) return next(new ErrorResponse('Resource not found', 404));

  res.json({
    success: true,
    data: {}
  });
});

// @route GET /api/v1/tbdocuments/tbdocid/:tbdocid
// @desc Get tbdocument by tbDocID
// @access  Public
exports.getTbDocByTbdocid = asyncHandler(async (req, res, next) => {
  const tbDoc = await Timebarsdocs.findOne({ tbDocID: req.params.tbdocid });

  if (!tbDoc) return next(new ErrorResponse('Resource not found', 404));

  res.json({
    success: true,
    data: tbDoc
  });
});

// @route DELETE /tbdocuments/tbdocid/:tbdocid
// @desc DELETE tbdocuments by tbdocid
// @access  Private
exports.deleteTbDocByTbdocid = asyncHandler(async (req, res, next) => {
  const deletedTbDoc = await Timebarsdocs.findOneAndDelete({ tbDocID: req.params.tbdocid });

  if (!deletedTbDoc) return next(new ErrorResponse('Resource not found', 404));

  res.json({
    success: true,
    data: {}
  });
});
