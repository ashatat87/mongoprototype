const express = require('express');
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
const path = require('path');
const fs = require('fs')
const dotenv = require('dotenv');
const logger = require('morgan');

const connectDB = require('./config/db');
const apiRouter = require('./routes');
const errorHandler = require('./middleware/error');

// Load env vars
dotenv.config({ path: './config/config.env' });

// Connect to MongoDB
connectDB();


const app = express();
const cors = require('cors')
app.use(cors())
app.use(logger('dev'));

app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(cookieParser())

// API routes
app.use('/api/v1', apiRouter);

/*--------------     start of Web Work -----------------*/

// load home page
app.get('/', (req, res) => {
  res.sendFile(path.resolve(__dirname, 'index.html'));
});

// jim custom stuff

app.use(express.static('./public'));
app.use(express.static('./scripts'));
app.use(express.static('.'));

// EJS
app.set('view engine', 'ejs');

app.use(express.static('./views'));
app.get('/upl', (req, res) => res.render('index'));

// quick route test 
app.get('/test', (req, res) => res.json({
  msg: 'root text app get route Works'

}));

app.get('/test2', function (req, res) {
  res.send('Hello World');
});

app.use(errorHandler);

const port = process.env.PORT || 3003;
app.listen(port, () => console.log(`Web Server running on port ${port}`));



