const fs = require('fs');
const mongoose = require('mongoose');
const colors = require('colors');
const dotenv = require('dotenv');

// Load env vars
dotenv.config({ path: './config/config.env' });

// Load models
const {
  User,
  Timebarstags,
  Timebarsrescalcs,
  TimebarsRes,
  Timebarsmetadata,
  Timebarscore,
  Timebarsbaseline,
  Timebarsadminpanel,
  Timebarsdocs
} = require('./models');

// Connect to DB
mongoose.connect(process.env.MONGO_URI, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false,
  useUnifiedTopology: true
});

const data = {
  users: 'users.json',
  timebars: 'tbdataTimebars.json',
  documents: 'tbdataDocuments.json',
  tags: 'tbdataTags.json',
  resources: 'tbdataResources.json',
  resCalcs: 'tbdataResCalcs.json',
  metadata: 'tbdataMetadata.json',
  baselines: 'tbdataBaseline.json',
  adminPanelConfigs: 'tbdataAdminPanelConfig.json',
}

/**
 * load JSON files into data object
 * @param {Object} data 
 * @returns {Object} return the loaded data
 */
function loadData(data) {
  const newData = { ...data };
  Object.keys(data).forEach(key => {
    const jsonData = fs.readFileSync(`${__dirname}/_data/${data[key]}`, 'utf-8');

    newData[key] = JSON.parse(jsonData);
  });
  return newData;
}

// Read JSON files
const { 
  users,
  timebars,
  documents,
  tags,
  resources,
  resCalcs,
  metadata,
  baselines,
  adminPanelConfigs,
 } = loadData(data);


// Import into DB
const importData = async () => {
  try {
    await User.create(users);
    await Timebarstags.create(tags);
    await Timebarsrescalcs.create(resCalcs);
    await TimebarsRes.create(resources);
    await Timebarsmetadata.create(metadata);
    await Timebarscore.create(timebars);
    await Timebarsbaseline.create(baselines);
    await Timebarsadminpanel.create(adminPanelConfigs);
    await Timebarsdocs.create(documents);

    console.log('Data Imported...'.green.inverse);
    process.exit();
  } catch (err) {
    console.error(err);
    process.exit();
  }
};

// Delete data
const deleteData = async () => {
  try {
    await User.collection.drop();
    await Timebarstags.collection.drop();
    await Timebarsrescalcs.collection.drop();
    await TimebarsRes.collection.drop();
    await Timebarsmetadata.collection.drop();
    await Timebarscore.collection.drop();
    await Timebarsbaseline.collection.drop();
    await Timebarsadminpanel.collection.drop();
    await Timebarsdocs.collection.drop();

    console.log('Data Destroyed...'.red.inverse);
    process.exit();
  } catch (err) {
    console.error(err);
    process.exit();
  }
};

if (process.argv[2] === '-i') {
  importData();
} else if (process.argv[2] === '-d') {
  deleteData();
}
