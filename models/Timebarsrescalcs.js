const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Schema
const tbResCalcsSchema = new Schema({

    tbResCalcID: {
        type: String,
        unique: true,
        required: true
    },
    tbResCalcTbResID: String,
    tbResCalcTbID: String,
    tbResCalcName: String,
    tbResCalcWeek: String,
    tbResCalcHours: String,
    tbResCalcWdays: String,
    tbResCalcStartWeekNo: String,
    tbResCalcTbWeekNo: String,
    tbResCalcWeekSumHours: String,
    tbResCalcWeekSumCost: String,
    tbResCalcWeek1: String,
    tbResCalcWeek2: String,
    tbResCalcWeek3: String,
    tbResCalcWeek4: String,
    tbResCalcWeek5: String,
    tbResCalcWeek6: String,
    tbResCalcWeek7: String,
    tbResCalcWeek8: String,
    tbResCalcWeek9: String,
    tbResCalcWeek10: String,
    tbResCalcWeek11: String,
    tbResCalcWeek12: String,
    tbResCalcWeek13: String,
    tbResCalcWeek14: String,
    tbResCalcWeek15: String,
    tbResCalcWeek16: String,
    tbResCalcWeek17: String,
    tbResCalcWeek18: String,
    tbResCalcWeek19: String,
    tbResCalcWeek20: String,
    tbResCalcWeek21: String,
    tbResCalcWeek22: String,
    tbResCalcWeek23: String,
    tbResCalcWeek24: String,
    tbResCalcWeek25: String,
    tbResCalcWeek26: String,
    tbResCalcWeek27: String,
    tbResCalcWeek28: String,
    tbResCalcWeek29: String,
    tbResCalcWeek30: String,
    tbResCalcWeek31: String,
    tbResCalcWeek32: String,
    tbResCalcAzureID: String,
    tbResCalcCustomerID: String,
    tbResCalcLastModified: String
});

// note tbrescalcs is the collection name
module.exports = mongoose.model('tbrescalcs', tbResCalcsSchema);
