const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Schema
const tbCoreSchema = new Schema({

    tbID: {
        type: String,
        unique: true,
        required: true
    },
    tbCustomerID: String,
    tbAzureID: String,
    tbName: String,
    tbL1: String,
    tbL2: String,
    tbL3: String,
    tbL4: String,
    tbL5: String,
    tbDuration: String,
    tbRemainingDuration: String,
    tbStart: String,
    tbFinish: String,
    tbOwner: String,
    tbCoordTop: String,
    tbCoordLeft: String,
    tbCoordRight: String,
    tbType:String,
    tbResID: String,
    tbMetaDataID: String,
    tbBLID: String,
    tbCalendar: String,
    tbPredecessor: String,
    tbAStart: String,
    tbAFinish: String,
    tbWork: String,
    tbAWork: String,
    tbWorkRemaining: String,
    tbPercentComplete: String,
    tbConstraintType: String,
    tbConstraintDate: String,
    tbFloat: String,
    tbFreeFloat: String,
    tbPercentTimeOn: String,
    tbExpHoursPerWeek: String,
    tbCostID: String,
    tbCost: String,
    tbCostRemaining: String,
    tbACost: String,
    tbPayRate: String,
    tbCostType: String,
    tbSelfKey2: String,
    tbBarColor: String,
    tbTextColor: String,
    kbCoordLeft: String,
    kbCoordTop: String,
    canvasNo: String,
    focdItemCoordLeft: String,
    focdItemCoordTop: String,
    tbHierarchyOrder: String,
});

// note tbcore is the collection name
module.exports = Timebarscore = mongoose.model('tbcore', tbCoreSchema);
