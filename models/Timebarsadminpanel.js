const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Schema
const tbAdminPanelSchema = new Schema({

    apID: {
        type: String,
        unique: true,
        required: true
    },
    apCustomerID: String,
    apAzureID: String,
    ap_name: String,
    apTS_WeeklyOrMonthly: String,
    apTS_Start: String,
    apTS_Finish: String,
    apTS_MonthlyPxFactor: String,
    apTS_WeeklyPxFactor: String,
    apHelpCoordTop: String,
    apHelpCoordLeft: String,
    apStatusDate: String,
    ap_HolidayDate: String,
    ap_HolidayName: String,
    totalNumberOfPeople: String,
    numberOfPeoplePerLine: String,
    pxPerPerson: String,
    apFilteredFrom: String,
    apFilteredFromTbID: String,
    apSchMethod: String,
    apTbVisibleLevel: String,
    hideCompletedBarsYN: String
});

// note tbadminpanel is the collection name
module.exports = mongoose.model('tbadminpanel', tbAdminPanelSchema);
