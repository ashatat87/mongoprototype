
Jm Testing Jan 14 2020

### get all by apSchMethod
GET http://{{host}}/api/v1/tbadminpanel?apSchMethod=Standard





# MongoPrototype
Prototype NodeJs app to build out a mongo db

# added Fields Files
```
to be used as master set of fields for each model
```

# Second Set of Requirements, Dec 14
```
read the "readmeAuthMiddlewareControllers", research and understand how Traversy put this together, note that i want to use
his logice for the users, auth, validation and error handling. So i put his code in the repo, have a look.

Requirements: Wire up and test the User Model, user controller, user route

Wire up and test the  6 middleware js files

wire up and test the auth and user routes to hook up the auth logic.

test the auth method for use on the documents route you wrote

then rewrite the documents routes to match the patterns used in the user routes (unless you have better advice)

look at how validation was implemented and make recommendations to continue this way or follow your patterns

remove the geocoder functionality everwhere

hook up the seeder, pump data into users and documents tables

```


# First Set of Requirements, Dec 11
Requirements: Build out 8 Mongoose models, routes and methods

```
note tbCore already has a route and some api methods, use them as starting point and pattern to follow

# the following are the names of the new models, below are fields and rules 
const STORE_ADMINPANEL = 'tbAdminPanel';
const STORE_TBCORE = 'tbCore'; 
const STORE_RESOURCES = 'tbResources';
const STORE_TAGS = 'tbTags';
const STORE_METADATA = 'tbMetaData';
const STORE_BASELINE = "tbBaseline";
const STORE_RESCALCS = "tbResCalcs";
const STORE_DOCS = 'tbDocuments';
```

# Create Docs Table
        let store = db.createObjectStore(STORE_DOCS, { keyPath: "id", autoIncrement: true });
        store.createIndex('tbDocID', 'tbDocID', { unique: true });
        store.createIndex('tbDocName', 'tbDocName', { unique: false });
        store.createIndex('tbDocCustomerID', 'tbDocCustomerID', { unique: false });
        store.createIndex('tbDocBriefDescription', 'tbDocBriefDescription', { unique: false });
        store.createIndex('tbDocType', 'tbDocType', { unique: false });

        store.createIndex('tbDocNameShort', 'tbDocNameShort', { unique: false });
        store.createIndex('tbDocHTMLContent', 'tbDocHTMLContent', { unique: false });
        store.createIndex('tbDocGroup', 'tbDocGroup', { unique: false });
        store.createIndex('tbDocPurpose', 'tbDocPurpose', { unique: false });
        store.createIndex('tbDocOwner', 'tbDocOwner', { unique: false });
        store.createIndex('tbDocPopular', 'tbDocPopular', { unique: false });
        store.createIndex('tbDocSortOrder', 'tbDocSortOrder', { unique: false });
        store.createIndex('tbDocLastModified', 'tbDocLastModified', { unique: false });
        store.createIndex('tbDocApprovedBy', 'tbDocApprovedBy', { unique: false });
        store.createIndex('tbDocStatus', 'tbDocStatus', { unique: false });
        store.createIndex('tbDocWrittenBy', 'tbDocWrittenBy', { unique: false });


# Create Baseline table or store
        store = db.createObjectStore(STORE_BASELINE, { keyPath: "id", autoIncrement: true });
        store.createIndex('tbID', 'tbID', { unique: true });
        store.createIndex('tbSelfKey2', 'tbSelfKey2', { unique: false });
        store.createIndex('tbBLID', 'tbBLID', { unique: false }); 
        store.createIndex('tbCustomerID', 'tbCustomerID', { unique: false });
        store.createIndex('tbMetaDataID', 'tbMetaDataID', { unique: false }); 
        store.createIndex('tbAzureID', 'tbAzureID', { unique: false });
        store.createIndex('tbName', 'tbName', { unique: false });



# create table to store resource calculations
        store = db.createObjectStore(STORE_RESCALCS, { keyPath: "id", autoIncrement: true });
        store.createIndex('tbResCalcID', 'tbResCalcID', { unique: true });
        store.createIndex('tbResCalcCustomerID', 'tbResCalcCustomerID', { unique: false });
        store.createIndex('tbResCalcAzureID', 'tbResCalcAzureID', { unique: false });
        store.createIndex('tbResCalcName', 'tbResCalcName', { unique: false });
        store.createIndex('tbResCalcWeek', 'tbResCalcWeek', { unique: false });
        store.createIndex('tbResCalcHours', 'tbResCalcHours', { unique: false });
        store.createIndex('tbResCalcResID', 'tbResCalcResID', { unique: false });
        store.createIndex('tbResCalcLastModified', 'tbResCalcLastModified', { unique: false });
        store.createIndex('tbResCalcWeek1', 'tbResCalcWeek1', { unique: false });
        store.createIndex('tbResCalcWeek2', 'tbResCalcWeek2', { unique: false });
        store.createIndex('tbResCalcWeek3', 'tbResCalcWeek3', { unique: false });
        store.createIndex('tbResCalcWeek4', 'tbResCalcWeek4', { unique: false });
        store.createIndex('tbResCalcWeek5', 'tbResCalcWeek5', { unique: false });
        store.createIndex('tbResCalcWeek6', 'tbResCalcWeek6', { unique: false });
        store.createIndex('tbResCalcWeek7', 'tbResCalcWeek7', { unique: false });
        store.createIndex('tbResCalcWeek8', 'tbResCalcWeek8', { unique: false });
        store.createIndex('tbResCalcWeek9', 'tbResCalcWeek9', { unique: false });
        store.createIndex('tbResCalcWeek10', 'tbResCalcWeek10', { unique: false });
        store.createIndex('tbResCalcWeek11', 'tbResCalcWeek11', { unique: false });
        store.createIndex('tbResCalcWeek12', 'tbResCalcWeek12', { unique: false });
        store.createIndex('tbResCalcWeek13', 'tbResCalcWeek13', { unique: false });
        store.createIndex('tbResCalcWeek14', 'tbResCalcWeek14', { unique: false });
        store.createIndex('tbResCalcWeek15', 'tbResCalcWeek15', { unique: false });
        store.createIndex('tbResCalcWeek16', 'tbResCalcWeek16', { unique: false });
        store.createIndex('tbResCalcWeek17', 'tbResCalcWeek17', { unique: false });
        store.createIndex('tbResCalcWeek18', 'tbResCalcWeek18', { unique: false });
        store.createIndex('tbResCalcWeek19', 'tbResCalcWeek19', { unique: false });
        store.createIndex('tbResCalcWeek20', 'tbResCalcWeek20', { unique: false });
        store.createIndex('tbResCalcWeek21', 'tbResCalcWeek21', { unique: false });
        store.createIndex('tbResCalcWeek22', 'tbResCalcWeek22', { unique: false });
        store.createIndex('tbResCalcWeek23', 'tbResCalcWeek23', { unique: false });
        store.createIndex('tbResCalcWeek24', 'tbResCalcWeek24', { unique: false });
        store.createIndex('tbResCalcWeek25', 'tbResCalcWeek25', { unique: false });
          store.createIndex('tbResCalcWeek26', 'tbResCalcWeek26', { unique: false });
        store.createIndex('tbResCalcWeek27', 'tbResCalcWeek27', { unique: false });
        store.createIndex('tbResCalcWeek28', 'tbResCalcWeek28', { unique: false });
        store.createIndex('tbResCalcWeek29', 'tbResCalcWeek29', { unique: false });
        store.createIndex('tbResCalcWeek30', 'tbResCalcWeek30', { unique: false });



# Create admin panel table (maybe see if key path can be changed to apID??
        store = db.createObjectStore(STORE_ADMINPANEL, { keyPath: "id", autoIncrement: true });
        store.createIndex('apID', 'apID', { unique: true });
        store.createIndex('apCustomerID', 'apCustomerID', { unique: false });
        store.createIndex('apAzureID', 'apAzureID', { unique: false });
        store.createIndex('ap_name', 'ap_name', { unique: false });
        store.createIndex('apTS_WeeklyOrMonthly', 'apTS_WeeklyOrMonthly', { unique: false });
        store.createIndex('apTS_Start', 'apTS_Start', { unique: false });
        store.createIndex('apTS_Finish', 'apTS_Finish', { unique: false });
        store.createIndex('apTS_MonthlyPxFactor', 'apTS_MonthlyPxFactor', { unique: false });
        store.createIndex('apTS_WeeklyPxFactor', 'apTS_WeeklyPxFactor', { unique: false });
        store.createIndex('apHelpCoordTop', 'apHelpCoordTop', { unique: false });
        store.createIndex('apHelpCoordLeft', 'apHelpCoordLeft', { unique: false });
        store.createIndex('apStatusDate', 'apStatusDate', { unique: false });  // added to forms July 30
        store.createIndex('ap_HolidayDate', 'ap_HolidayDate', { unique: false });  // not added to forms
        store.createIndex('ap_HolidayName', 'ap_HolidayName', { unique: false });  // not added to forms
        store.createIndex('totalNumberOfPeople', 'totalNumberOfPeople', { unique: false });  // not added to forms
        store.createIndex('numberOfPeoplePerLine', 'numberOfPeoplePerLine', { unique: false });  // not added to forms
        store.createIndex('pxPerPerson', 'pxPerPerson', { unique: false });  // not added to forms
        store.createIndex('apFilteredFrom', 'apFilteredFrom', { unique: false });  // not added to forms
        store.createIndex('apFilteredFromTbID', 'apFilteredFromTbID', { unique: false });  // not added to forms
        store.createIndex('apSchMethod', 'apSchMethod', { unique: false });  // not added to forms
        store.createIndex('apTbVisibleLevel', 'apTbVisibleLevel', { unique: false });  // not added to forms
        store.createIndex('hideCompletedBarsYN', 'hideCompletedBarsYN', { unique: false });  // not added to forms


# Create Timebars table (may want to put baseline in its own table later)
        store = db.createObjectStore(STORE_TBCORE, { keyPath: "id", autoIncrement: true });
        store.createIndex('tbID', 'tbID', { unique: true });
        store.createIndex('tbCustomerID', 'tbCustomerID', { unique: false });
        store.createIndex('tbAzureID', 'tbAzureID', { unique: false });
        store.createIndex('tbName', 'tbName', { unique: false });
        store.createIndex('tbL1', 'tbL1', { unique: false });
        store.createIndex('tbL2', 'tbL2', { unique: false });
        store.createIndex('tbL3', 'tbL3', { unique: false });
        store.createIndex('tbL4', 'tbL4', { unique: false });
        store.createIndex('tbL5', 'tbL5', { unique: false });
        store.createIndex('tbDuration', 'tbDuration', { unique: false });
        store.createIndex('tbRemainingDuration', 'tbRemainingDuration', { unique: false });
        store.createIndex('tbStart', 'tbStart', { unique: false });
        store.createIndex('tbFinish', 'tbFinish', { unique: false });
        store.createIndex('tbOwner', 'tbOwner', { unique: false });
        store.createIndex('tbCoordTop', 'tbCoordTop', { unique: false });
        store.createIndex('tbCoordLeft', 'tbCoordLeft', { unique: false });
        store.createIndex('tbCoordRight', 'tbCoordRight', { unique: false });
        store.createIndex('tbType', 'tbType', { unique: false });  //bLnk
        store.createIndex('tbResID', 'tbResID', { unique: false });
        store.createIndex('tbMetaDataID', 'tbMetaDataID', { unique: false }); 
        store.createIndex('tbBLID', 'tbBLID', { unique: false });
        store.createIndex('tbCalendar', 'tbCalendar', { unique: false }); // = work hours per day
        store.createIndex('tbPredecessor', 'tbPredecessor', { unique: false });
        store.createIndex('tbAStart', 'tbAStart', { unique: false });
        store.createIndex('tbAFinish', 'tbAFinish', { unique: false });
        store.createIndex('tbWork', 'tbWork', { unique: false });
        store.createIndex('tbAWork', 'tbAWork', { unique: false });  //blnk
        store.createIndex('tbWorkRemaining', 'tbWorkRemaining', { unique: false });
        store.createIndex('tbPercentComplete', 'tbPercentComplete', { unique: false });  // blnk
        store.createIndex('tbConstraintType', 'tbConstraintType', { unique: false });
        store.createIndex('tbConstraintDate', 'tbConstraintDate', { unique: false });
        store.createIndex('tbFloat', 'tbFloat', { unique: false });
        store.createIndex('tbFreeFloat', 'tbFreeFloat', { unique: false });
        store.createIndex('tbPercentTimeOn', 'tbPercentTimeOn', { unique: false });
        store.createIndex('tbExpHoursPerWeek', 'tbExpHoursPerWeek', { unique: false });
        store.createIndex('tbCostID', 'tbCostID', { unique: false });
        store.createIndex('tbCost', 'tbCost', { unique: false });
        store.createIndex('tbCostRemaining', 'tbCostRemaining', { unique: false });
        store.createIndex('tbACost', 'tbACost', { unique: false });
        store.createIndex('tbPayRate', 'tbPayRate', { unique: false });
        store.createIndex('tbCostType', 'tbCostType', { unique: false });
        store.createIndex('tbSelfKey2', 'tbSelfKey2', { unique: false });
        store.createIndex('tbBarColor', 'tbBarColor', { unique: false });
        store.createIndex('tbTextColor', 'tbTextColor', { unique: false });
        // 36 here now

 these are more new fields
            kbCoordLeft
            kbCoordTop
            canvasNo
            focdItemCoordLeft


# Create Resource Table
        store = db.createObjectStore(STORE_RESOURCES, { keyPath: "id", autoIncrement: true });
        store.createIndex('tbResID', 'tbResID', { unique: true });
        store.createIndex('tbResCustomerID', 'tbResCustomerID', { unique: false });
        store.createIndex('tbResAzureID', 'tbResAzureID', { unique: false });
        store.createIndex('tbResName', 'tbResName', { unique: true });
        store.createIndex('tbResNameShort', 'tbResNameShort', { unique: false });
        store.createIndex('tbResPrimarySkill', 'tbResPrimarySkill', { unique: false });
        store.createIndex('tbResPrimaryRole', 'tbResPrimaryRole', { unique: false });
        store.createIndex('tbResDepartment', 'tbResDepartment', { unique: false });
        store.createIndex('tbResManager', 'tbResManager', { unique: false });
        store.createIndex('tbResPayRate', 'tbResPayRate', { unique: false });
        store.createIndex('tbResCoordTop', 'tbResCoordTop', { unique: false });
        store.createIndex('tbResCoordLeft', 'tbResCoordLeft', { unique: false });
        store.createIndex('tbResTeam', 'tbResTeam', { unique: false });
        store.createIndex('tbResLocation', 'tbResLocation', { unique: false });
        store.createIndex('tbResCostCode', 'tbResCostCode', { unique: false });
        store.createIndex('tbResTeamLeader', 'tbResTeamLeader', { unique: false });
        store.createIndex('tbResSupervisor', 'tbResSupervisor', { unique: false });
        store.createIndex('tbResPartTimeFullTime', 'tbResPartTimeFullTime', { unique: false });
        store.createIndex('tbResResourceType', 'tbResResourceType', { unique: false });
        store.createIndex('tbResResourceClass', 'tbResResourceClass', { unique: false });
        store.createIndex('tbResResourceCalendar', 'tbResResourceCalendar', { unique: false });
        store.createIndex('tbResPercentGeneralAvailability', 'tbResPercentGeneralAvailability', { unique: false });
        store.createIndex('tbResExtSystemResID', 'tbResExtSystemResID', { unique: false });
        store.createIndex('tbResSortOrder', 'tbResSortOrder', { unique: false });
        store.createIndex('tbResLastModified', 'tbResLastModified', { unique: false });
        store.createIndex('tbResLabourType', 'tbResLabourType', { unique: false });



# Create Tags Table
        store = db.createObjectStore(STORE_TAGS, { keyPath: "id", autoIncrement: true });
        store.createIndex('tbTagID', 'tbTagID', { unique: true });
        store.createIndex('tbTagCustomerID', 'tbTagCustomerID', { unique: false });
        store.createIndex('tbTagAzureID', 'tbTagAzureID', { unique: false });
        store.createIndex('tbTagGroup', 'tbTagGroup', { unique: false });
        store.createIndex('tbTagName', 'tbTagName', { unique: false });
        store.createIndex('tbTagNameShort', 'tbTagNameShort', { unique: false });
        store.createIndex('tbTagPurpose', 'tbTagPurpose', { unique: false });
        store.createIndex('tbTagDescription', 'tbTagDescription', { unique: false });
        store.createIndex('tbTagOwner', 'tbTagOwner', { unique: false });
        store.createIndex('tbTagPopular', 'tbTagPopular', { unique: false });
        store.createIndex('tbTagSortOrder', 'tbTagSortOrder', { unique: false });
        store.createIndex('tbTagLastModified', 'tbTagLastModified', { unique: false });



# Create Metadata Table
        store = db.createObjectStore(STORE_METADATA, { keyPath: "id", autoIncrement: true });
        store.createIndex('tbMDID', 'tbMDID', { unique: true }); // for storing the tbkey from Timebars Table to allow one and only one meta data row for each tb row
        store.createIndex('canvasNo', 'canvasNo', { unique: false });
        store.createIndex('tbMDRefID', 'tbMDRefID', { unique: false });
        store.createIndex('tbMDCustomerID', 'tbMDCustomerID', { unique: false });
        store.createIndex('tbMDAzureID', 'tbMDAzureID', { unique: false });
        store.createIndex('tbMDName', 'tbMDName', { unique: false });
        store.createIndex('tbMDNameShort', 'tbMDNameShort', { unique: false });
        store.createIndex('tbMDProjectNumber', 'tbMDProjectNumber', { unique: false });
        store.createIndex('tbMDDescription', 'tbMDDescription', { unique: false });
        store.createIndex('tbMDNotes', 'tbMDNotes', { unique: false });
        store.createIndex('tbMDExtLink1', 'tbMDExtLink1', { unique: false });
        store.createIndex('tbMDExtSystemID1', 'tbMDExtSystemID1', { unique: false });
        store.createIndex('tbMDSortOrder', 'tbMDSortOrder', { unique: false });
        store.createIndex('tbMDtbLastModified', 'tbMDtbLastModified', { unique: false });
        store.createIndex('tbMDPriority', 'tbMDPriority', { unique: false });
        store.createIndex('tbMDStatus', 'tbMDStatus', { unique: false });
        store.createIndex('tbMDState', 'tbMDState', { unique: false });
        store.createIndex('tbMDSeverity', 'tbMDSeverity', { unique: false });
        store.createIndex('tbMDStage', 'tbMDStage', { unique: false });
        store.createIndex('tbMDPhase', 'tbMDPhase', { unique: false });
        store.createIndex('tbMDCategory', 'tbMDCategory', { unique: false });
        store.createIndex('tbMDHealth', 'tbMDHealth', { unique: false });
        store.createIndex('tbMDResponsibility', 'tbMDResponsibility', { unique: false });
        store.createIndex('tbMDDepartment', 'tbMDDepartment', { unique: false });
        store.createIndex('tbMDExSponsor', 'tbMDExSponsor', { unique: false });
        store.createIndex('tbMDPM', 'tbMDPM', { unique: false });
        store.createIndex('tbMDProjectType', 'tbMDProjectType', { unique: false });
        store.createIndex('tbMDShowIn', 'tbMDShowIn', { unique: false });
        store.createIndex('tbMDYesNoSelector', 'tbMDYesNoSelector', { unique: false });
        store.createIndex('tbMDProduct', 'tbMDProduct', { unique: false });
        store.createIndex('tbMDContact', 'tbMDContact', { unique: false });
        store.createIndex('tbMDWBS', 'tbMDWBS', { unique: false });
        store.createIndex('tbMDWeighting', 'tbMDWeighting', { unique: false });
        store.createIndex('tbMDLocation', 'tbMDLocation', { unique: false });
        store.createIndex('tbMDPrimarySkill', 'tbMDPrimarySkill', { unique: false });
        store.createIndex('tbMDPrimaryRole', 'tbMDPrimaryRole', { unique: false });
        store.createIndex('tbMDOther2', 'tbMDOther2', { unique: false });
        store.createIndex('tbMDOther3', 'tbMDOther3', { unique: false });
        store.createIndex('tbMDOther4', 'tbMDOther4', { unique: false });
        store.createIndex('tbMDGate', 'tbMDGate', { unique: false });
        store.createIndex('tbMDHealthOverall', 'tbMDHealthOverall', { unique: false })
        store.createIndex('tbMDHealthScope', 'tbMDHealthScope', { unique: false });;
        store.createIndex('tbMDHealthCost', 'tbMDHealthCost', { unique: false });
        store.createIndex('tbMDHealthIssues', 'tbMDHealthIssues', { unique: false });
        store.createIndex('tbMDHealthRisk', 'tbMDHealthRisk', { unique: false });
        store.createIndex('tbMDHealthSchedule', 'tbMDHealthSchedule', { unique: false });
        store.createIndex('tbMDHealthHours', 'tbMDHealthHours', { unique: false });
        store.createIndex('tbMDInvestmentCategory', 'tbMDInvestmentCategory', { unique: false });
        store.createIndex('tbMDInvestmentInitiative', 'tbMDInvestmentInitiative', { unique: false });
        store.createIndex('tbMDInvestmentObjective', 'tbMDInvestmentObjective', { unique: false });
        store.createIndex('tbMDInvestmentStrategy', 'tbMDInvestmentStrategy', { unique: false });
        store.createIndex('tbMDROMEstimate', 'tbMDROMEstimate', { unique: false });
        store.createIndex('tbMDPortfolio', 'tbMDPortfolio', { unique: false });
        store.createIndex('tbMDProgram', 'tbMDProgram', { unique: false });
        store.createIndex('tbMDProgActivityAlignment', 'tbMDProgramActivityAlignment', { unique: false });
        store.createIndex('tbMDSize', 'tbMDSize', { unique: false });
        store.createIndex('tbMDStageApprover', 'tbMDStageApprover', { unique: false });
        store.createIndex('tbMDWrittenBy', 'tbMDWrittenBy', { unique: false });
        store.createIndex('tbMDBusinessAdvisor', 'tbMDBusinessAdvisor', { unique: false });
        store.createIndex('tbMDBusinessOwner', 'tbMDBusinessOwner', { unique: false });
        store.createIndex('tbMDDeliveryManager', 'tbMDDeliveryManager', { unique: false });
        store.createIndex('tbMDOrgManager', 'tbMDOrgManager', { unique: false });
        store.createIndex('tbMDPriorityStrategic', 'tbMDPriorityStrategic', { unique: false });
        store.createIndex('tbMDSponsoringDepartment', 'tbMDSponsoringDepartment', { unique: false });
        store.createIndex('tbMDPrimaryContact', 'tbMDPrimaryContact', { unique: false });
        store.createIndex('tbMDBenefitCostRatio', 'tbMDBenefitCostRatio', { unique: false });
        store.createIndex('tbMDtbMDContactNumber', 'tbMDtbMDContactNumber', { unique: false });
        store.createIndex('tbMDResponsibleTeam', 'tbMDResponsibleTeam', { unique: false });
        store.createIndex('tbMDRiskVsSizeAndComplexity', 'tbMDRiskVsSizeAndComplexity', { unique: false });
        store.createIndex('tbMDtbMDEcnomicValueAdded', 'tbMDtbMDEcnomicValueAdded', { unique: false });
        store.createIndex('tbMDEstimationClass', 'tbMDEstimationClass', { unique: false });
        store.createIndex('tbMDInternalRateOfReturn', 'tbMDtbMDInternalRateOfReturn', { unique: false });
        store.createIndex('tbMDSprintName', 'tbMDtbMDSprintName', { unique: false });
        store.createIndex('tbMDSunkCosts', 'tbMDtbMDSunkCosts', { unique: false });
        store.createIndex('tbMDSyncNotes', 'tbMDSyncNotes', { unique: false });
        store.createIndex('tbMDNotesProject', 'tbMDNotesProject', { unique: false });
        store.createIndex('tbMDtbMDContractNumber', 'tbMDtbMDContractNumber', { unique: false });
        store.createIndex('tbMDNetPresentValue', 'tbMDNetPresentValue', { unique: false });
        store.createIndex('tbMDOpportunityCost', 'tbMDOpportunityCost', { unique: false });
        store.createIndex('tbMDPaybackPeriod', 'tbMDPaybackPeriod', { unique: false });
        store.createIndex('tbMDPrimaryLineOfBusiness', 'tbMDPrimaryLineOfBusiness', { unique: false });
        store.createIndex('tbMDBackgroundInfo', 'tbMDBackgroundInfo', { unique: false });
        store.createIndex('tbMDCapabilitiesNeeded', 'tbMDCapabilitiesNeeded', { unique: false });
        store.createIndex('tbMDConsequence', 'tbMDConsequence', { unique: false });
        store.createIndex('tbMDExpectedBenfits', 'tbMDExpectedBenfits', { unique: false });
        store.createIndex('tbMDProblemOpportunity', 'tbMDProblemOpportunity', { unique: false });
        store.createIndex('tbMDConstraintsAssumptions', 'tbMDConstraintsAndAssumptions', { unique: false });
        store.createIndex('tbMDCostBenefitAnalysis', 'tbMDCostBenefitAnalysis', { unique: false });
        store.createIndex('tbMDExecutiveSummary', 'tbMDExecutiveSummary', { unique: false });
        store.createIndex('tbMDSeniorLevelCommittment', 'tbMDSeniorLevelCommittment', { unique: false });
        store.createIndex('tbMDStakeholderDescription', 'tbMDStakeholderDescription', { unique: false });
        store.createIndex('tbMDNotesWorkflow', 'tbMDNotesWorkflow', { unique: false });
        store.createIndex('tbMDOther5', 'tbMDOther5', { unique: false });
        store.createIndex('tbMDOther6', 'tbMDOther6', { unique: false });
        store.createIndex('tbMDOther7', 'tbMDOther7', { unique: false });
        store.createIndex('tbMDOther8', 'tbMDOther8', { unique: false });
        store.createIndex('tbMDOther9', 'tbMDOther9', { unique: false });
        store.createIndex('tbMDOther10', 'tbMDOther10', { unique: false });
        store.createIndex('tbMDOther11', 'tbMDOther11', { unique: false });
        store.createIndex('tbMDOther12', 'tbMDOther12', { unique: false });
        store.createIndex('tbMDOther13', 'tbMDOther13', { unique: false });
        store.createIndex('tbMDOther14', 'tbMDOther14', { unique: false });
        store.createIndex('tbMDOther15', 'tbMDOther15', { unique: false });
